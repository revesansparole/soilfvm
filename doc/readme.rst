Overview
========

.. {# pkglts, glabpkg

.. image:: https://revesansparole.gitlab.io/soilfvm/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://revesansparole.gitlab.io/soilfvm/

.. image:: https://revesansparole.gitlab.io/soilfvm/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/soilfvm/0.0.1/

.. image:: https://revesansparole.gitlab.io/soilfvm/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/soilfvm

.. image:: https://badge.fury.io/py/soilfvm.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/soilfvm


main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/revesansparole/soilfvm/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/revesansparole/soilfvm/commits/main

.. |main_coverage| image:: https://gitlab.com/revesansparole/soilfvm/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/revesansparole/soilfvm/commits/main


prod: |prod_build|_ |prod_coverage|_

.. |prod_build| image:: https://gitlab.com/revesansparole/soilfvm/badges/prod/pipeline.svg
.. _prod_build: https://gitlab.com/revesansparole/soilfvm/commits/prod

.. |prod_coverage| image:: https://gitlab.com/revesansparole/soilfvm/badges/prod/coverage.svg
.. _prod_coverage: https://gitlab.com/revesansparole/soilfvm/commits/prod

.. #}

Implementation of Finite Volume Methods to solve soil related formalisms
