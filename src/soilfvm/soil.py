"""
Soil structure:
 - Set of soil voxels
 - Topological relations between voxels as a directed graph
 - Properties of connections between voxels (e.g. contact surface area)
"""
from dataclasses import dataclass

import networkx as nx


@dataclass
class VoxelBase:
    altitude: float = 0.
    """[m] altitude of voxel (negative, 0 on surface)
    """

    volume: float = 0.
    """[m3] volume of soil voxel
    """

    theta_sat: float = 0.
    """[m3.m-3] soil moisture at saturation
    """

    theta_fc: float = 0.
    """[m3.m-3] soil moisture at field capacity
    """

    theta_pwp: float = 0.
    """[m3.m-3] soil moisture at plant wilting point
    """

    theta_res: float = 0.
    """[m3.m-3] residual soil moisture
    """

    def kw(self, theta):
        """Water conductance

        Args:
            theta (float): [m3.m-3] soil moisture

        Returns:
            (float): [m3.m-1.s-1.MPa-1]
        """
        raise NotImplementedError

    def psi(self, theta):
        """Water potential

        Args:
            theta (float): [m3.m-3] soil moisture

        Returns:
            (float): [MPa]
        """
        raise NotImplementedError


@dataclass
class EdgeBase:
    length: float = 0.
    """[m] equivalent distance between two voxels
    """

    surface: float = 0.
    """[m2] contact surface area between two voxels
    """


class Soil:

    def __init__(self):
        self.topo = nx.DiGraph()

    def voxels(self):
        """Iterate on all voxels

        Returns:
            (nid, VoxelBase): node id and its properties
        """
        for node in self.topo.nodes:
            if node not in ('atm', 'deep'):
                yield node, self.topo.nodes[node]['object']

    def edges(self):
        """Iterate on all connexions between voxels

        Returns:
            (eid, EdgeBase): edge id and its properties
        """
        for edge in self.topo.edges:
            yield edge, self.topo.edges[edge]['object']

    def voxel(self, vid):
        """Properties associated to given voxel.

        Args:
            vid (Node): voxel id

        Returns:
            (VoxelBase): voxel properties
        """
        return self.topo.nodes[vid]['object']
