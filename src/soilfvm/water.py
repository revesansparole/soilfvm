"""
Water balance formalism to compute soil moisture
"""
from math import sqrt


def edge_fluxes(soil, theta, surface_water):
    """Water fluxes across surfaces between voxels.

    Args:
        soil (Soil):
        theta (dict): [m3.m-3] soil moisture in each voxel
        surface_water (dict): Amount of water stored at the interface between atm and soil

    Returns:
        (dict): [m3.s-1] flux of water through surface of voxels from source towards target
    """
    flux = {}
    for edge, prop in soil.edges():
        sid, tid = edge
        if sid == 'atm':
            if surface_water[edge] <= 0:
                flux[edge] = 0
            else:
                vox_tgt = soil.voxel(tid)
                psi_tgt = vox_tgt.psi(theta[tid])

                infiltration_rate = vox_tgt.kw(vox_tgt.theta_sat) * (0. - psi_tgt) / prop.length
                # use theta_sat instead of theta to simulate cracks on the surface and rapid infiltration
                # infiltration_rate = vox_tgt.kw(theta[edge[1]]) * (0. - psi_tgt) / prop.length

                flux[edge] = infiltration_rate
        elif tid == 'deep':
            vox_src = soil.voxel(sid)
            psi_src = vox_src.psi(theta[sid])
            psi_deep = vox_src.psi(vox_src.theta_fc)
            if psi_src <= psi_deep:
                flux[edge] = 0.
            else:
                flux[edge] = vox_src.kw(theta[sid]) * (psi_src - psi_deep) / prop.length

        else:
            vox_src = soil.voxel(sid)
            psi_src = vox_src.psi(theta[sid])

            vox_tgt = soil.voxel(tid)
            psi_tgt = vox_tgt.psi(theta[tid])

            kw = sqrt(vox_src.kw(theta[sid]) * vox_tgt.kw(theta[tid]))
            flux[edge] = kw * (psi_src - psi_tgt) / prop.length

    return flux


def vol_fluxes(soil, theta, kroot):
    """Water fluxes happening inside voxel volume.

    Args:
        soil (Soil):
        theta (dict): [m3.m-3] soil moisture in each voxel
        kroot (List[(float, float)]): ([m3.s-1.MPa-1], [MPa]) tuple of root conductance,
                                      water potential for each plant in each voxel.

    Returns:
        (dict): [m3.s-1] flux of water entering each voxel
    """
    flux = {}
    for node, vox in soil.voxels():
        uptake = 0
        for k, psi in kroot[node]:
            uptake -= k * (vox.psi(theta[node]) - psi)

        flux[node] = -uptake

    return flux


def balance(soil, theta, surface_water, kroot, time_step, delta_theta_th=1e-3):
    """Fluxes of water entering voxels.

    Args:
        soil (Soil):
        theta (dict): [m3.m-3] soil moisture in each voxel
        surface_water (dict): Amount of water stored at the interface between
                              atm and soil
        kroot (List[(float, float)]): ([m3.s-1.MPa-1], [MPa]) tuple of root conductance,
                                      water potential for each plant in each voxel.
        time_step (float): [s] time step
        delta_theta_th (float): maximum allowed change over one computation of fluxes

    Returns:
        (dict, dict, float): [m3]
          - amount of water entering each voxel for given time step.
          - amount of water change at surface for given time step.
          - drainage.
    """
    theta = dict(theta)  # save value since modified by algorithm
    surface_water = dict(surface_water)  # save value since modified by algorithm

    vox_delta = {node: 0. for node, _ in soil.voxels()}
    surf_delta = {edge: 0. for edge in surface_water}
    drainage = 0  # [m3] amount of water drain at the bottom of column

    sub_t = 0
    sub_dt = time_step
    while sub_t < time_step:
        # compute fluxes
        flux_surf = edge_fluxes(soil, theta, surface_water)
        flux_vol = vol_fluxes(soil, theta, kroot)

        # compute balance for each node
        dtheta_rate = {}
        for node, vox in soil.voxels():
            bal = flux_vol[node]

            for edge in soil.topo.out_edges(node):
                bal -= flux_surf[edge]

            for edge in soil.topo.in_edges(node):
                bal += flux_surf[edge]

            dtheta_rate[node] = bal / vox.volume

        # limit amount of change per voxel
        max_dthr = max(delta_theta_th / time_step,
                       max(abs(dthr) for dthr in dtheta_rate.values()))
        dt_th = delta_theta_th / max_dthr
        if sub_dt > dt_th:
            sub_dt = dt_th

        # carry out water balance
        for edge in soil.topo.out_edges('atm'):
            residual = surface_water[edge] - flux_surf[edge] * sub_dt

            if residual >= 0:
                surf_delta[edge] -= flux_surf[edge] * sub_dt
                surface_water[edge] = residual
            else:  # numerical instabilities?
                assert residual > -1e-3
                surf_delta[edge] -= surface_water[edge]
                surface_water[edge] = 0.

        for edge in soil.topo.in_edges('deep'):
            drainage += flux_surf[edge] * sub_dt

        for node, dthr in dtheta_rate.items():
            theta[node] += dthr * sub_dt
            vox_delta[node] += dthr * sub_dt

        sub_t += sub_dt
        sub_dt = time_step - sub_t

    return vox_delta, surf_delta, drainage
