"""
Construct typical soil
"""
from .soil import Soil, VoxelBase, EdgeBase


def column_1d(depths, voxel_cls=VoxelBase, edge_cls=EdgeBase):
    """Simple 1D vertical soil.

    The soil will be connected to 'atm' on top and
    'deep' at the bottom

    Args:
        depths (List[float]): [m] depth of each horizon
        voxel_cls (Type): class to instantiate for each voxel (subclass of VoxelBase)
        edge_cls (Type): class to instantiate for each edge (subclass of EdgeBase)

    Returns:
        (Soil)
    """
    surf = 1  # [m2] surface of columns on top

    soil = Soil()
    topo = soil.topo

    topo.add_node('atm')
    topo.add_node('deep')

    pid = 'atm'
    alt = 0.
    for i, depth in enumerate(depths):
        if i > 0:
            length = depth / 2 + depths[i - 1] / 2
        else:
            length = depth

        topo.add_node(i, object=voxel_cls(altitude=alt - depth / 2, volume=depth * surf))
        topo.add_edge(pid, i, object=edge_cls(length=length, surface=surf))
        pid = i
        alt -= depth

    topo.add_edge(pid, 'deep', object=edge_cls(length=depths[-1], surface=surf))

    return soil
