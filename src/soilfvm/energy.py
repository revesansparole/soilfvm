"""
Energy balance formalism to compute temperatures
"""
from math import sqrt


def edge_fluxes(soil, temp, surface_energy, theta):
    """Heat fluxes across surfaces between voxels.

    Args:
        soil (Soil):
        temp (dict): [°C] temperature in each voxel
        surface_energy (Function): Flux of energy at the interface between atm and soil
        theta (dict): [m3.m-3] soil moisture in each voxel

    Returns:
        (dict): [W] flux of heat through surface of voxels from source toward target
    """
    flux = {}
    for edge, prop in soil.edges():
        sid, tid = edge
        surf = prop.surface
        if sid == 'atm':
            flux[edge] = surface_energy(temp[tid]) * surf
        elif tid == 'deep':
            kth = soil.voxel(sid).kth(theta[sid])
            flux[edge] = kth * (temp[sid] - temp[tid]) / prop.length
        else:
            kth = sqrt(soil.voxel(sid).kth(theta[sid]) * soil.voxel(tid).kth(theta[tid]))
            flux[edge] = kth * (temp[sid] - temp[tid]) / prop.length

    return flux


def vol_fluxes(soil, temp):
    """Heat fluxes happening inside voxel volume.

    Args:
        soil (Soil):
        temp (dict): [°C] temperature in each voxel

    Returns:
        (dict): [W] flux of heat entering each voxel
    """
    flux = {node: 0. for node, _ in soil.voxels()}

    return flux


def balance(soil, temp, surface_energy, time_step, theta=None, delta_temp_th=1e-1):
    """Fluxes of heat entering voxels

    Args:
        soil (Soil):
        temp (dict): [°C] temperature in each voxel
        surface_energy (Function): Flux of energy at the interface between atm and soil
        time_step (float): [s] time step
        theta (dict): [m3.m-3] soil moisture in each voxel
        delta_temp_th (float): maximum allowed change over one computation of fluxes

    Returns:
        (dict): [°C] Temperature variation in each voxel
    """
    temp = dict(temp)  # save value since modified by algorithm
    if theta is None:
        theta = {vid: 0. for vid in temp}

    vox_delta = {node: 0. for node, _ in soil.voxels()}

    sub_t = 0
    sub_dt = time_step
    while sub_t < time_step:
        flux_surf = edge_fluxes(soil, temp, surface_energy, theta)
        flux_vol = vol_fluxes(soil, temp)

        # compute exchange for each node
        dtemp_rate = {}
        for node, vox in soil.voxels():
            bal = flux_vol[node]

            for edge in soil.topo.out_edges(node):
                bal -= flux_surf[edge]

            for edge in soil.topo.in_edges(node):
                bal += flux_surf[edge]

            dtemp_rate[node] = bal / vox.heat_capacity(theta[node])

        # limit amount of change per voxel
        max_dtr = max(delta_temp_th / time_step,
                      max(abs(dtr) for dtr in dtemp_rate.values()))
        dt_th = delta_temp_th / max_dtr
        if sub_dt > dt_th:
            sub_dt = dt_th

        # carry out temperature change
        for node, dtr in dtemp_rate.items():
            temp[node] += dtr * sub_dt
            vox_delta[node] += dtr * sub_dt

        sub_t += sub_dt
        sub_dt = time_step - sub_t

    return vox_delta
