"""
Plot effect of discretization
=============================

Perform same simulation on one soil discretized with different number of layers
"""
from datetime import datetime, timedelta
from math import log, radians

import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import pandas as pd
from duffie2013 import clear_sky
from soilfvm.energy import balance
from soilfvm.soil_factory import column_1d

from common import Voxel
from constants import air_density, air_heat_capacity, black_body, von_karman

day = datetime(2015, 7, 8)
per = pd.Period(day, '2D')

latitude = radians(43)
longitude = radians(4)

ws = 1.5  # [m.s-1] Wind speed min/max=0.6 [m.s−1] / 4.7 [m.s−1]
t_atm = 20  # [°C]

hc = 1.5  # [m] canopy height
z_m = 2  # [m]
albedo = 0.1  # [-]
emissivity = 0.95  # [-]


def atm_exchange(t_soil, rg_ground, t_atm, ws):
    """Energy exchange with atmosphere

    Args:
        t_soil (float): [°C] temperature of soil surface
        rg_ground (float): [W.m-2] incoming amount of radiation flat on the ground
        t_atm (float): [°C] temperature of atmosphere
        ws (float): [m.s-1] wind speed

    Returns:
        (float): [W.m-2] positive if energy entering the soil
    """
    s_t_vis = rg_ground / 2
    s_t_ir = rg_ground / 2 + black_body(t_atm)

    s_net = s_t_vis * (1 - albedo) + s_t_ir * emissivity - black_body(t_soil) * emissivity

    d = 2 / 3 * hc
    zom = 0.123 * hc
    g_atm = von_karman ** 2 * ws / log((z_m - d) / zom) / log((z_m - d) / (0.1 * zom))  # [m.s-1]
    sensible = air_density(t_atm) * air_heat_capacity * g_atm * (t_soil - t_atm)

    return s_net - sensible


soil_depth = 1  # [m] overall soil depth
nb = 10  # [#] number of extra horizons below surface horizon
soil = column_1d([0.05] + [(soil_depth - 0.05) / nb] * nb, Voxel)

for node, vox in soil.voxels():
    vox.from_texture(clay_fraction=0.1, sand_fraction=0.65)  # sandy loam
    # vox.from_texture(clay_fraction=0.35, sand_fraction=0.1)  # silty clay loam

nb = nb // 2
soil_large = column_1d([0.05] + [(soil_depth - 0.05) / nb] * nb, Voxel)

for node, vox in soil_large.voxels():
    vox.from_texture(clay_fraction=0.1, sand_fraction=0.65)  # sandy loam
    # vox.from_texture(clay_fraction=0.35, sand_fraction=0.1)  # silty clay loam

t_soil_deep = 15  # [°C] temperature of deep soil

temp = {node: t_soil_deep for node, _ in soil.voxels()}
temp['deep'] = t_soil_deep
temp_var = {node: 0. for node, _ in soil.voxels()}

temp_large = {node: t_soil_deep for node, _ in soil_large.voxels()}
temp_large['deep'] = t_soil_deep
temp_var_large = {node: 0. for node, _ in soil_large.voxels()}

dt = 10 * 60  # '10min' in [s]

records = []
date = day
while date <= per.end_time:
    # update state of model
    date += timedelta(seconds=dt)
    for node, _ in soil.voxels():
        temp[node] += temp_var[node]
    for node, _ in soil_large.voxels():
        temp_large[node] += temp_var_large[node]

    # recompute new fluxes for this time step
    bla = 1  # bug pycharm in reformat code


    def surf_exch(t_soil):
        return atm_exchange(t_soil, sum(clear_sky.rg_ground(date, latitude, longitude)), t_atm, ws)


    temp_var = balance(soil, temp, surf_exch, dt)
    temp_var_large = balance(soil_large, temp_large, surf_exch, dt)

    entry = dict(date=date)
    for vid, _ in soil.voxels():
        entry[f'st_{vid:02d}'] = temp[vid]
    for vid, _ in soil_large.voxels():
        entry[f'stl_{vid:02d}'] = temp_large[vid]

    records.append(entry)

df = pd.DataFrame(records).set_index('date')

# plot result
fig, axes = plt.subplots(1, 1, sharex='all', figsize=(11, 5), squeeze=False)
ax = axes[0, 0]
ax.set_title(f"{day.date().isoformat()}")

for vid, _ in tuple(sorted(soil_large.voxels()))[1:]:
    crv, = ax.plot(df.index, df[f'stl_{vid:02d}'], '-', label=f"t_soil_{vid:d}")
    ax.plot(df.index, (df[f'st_{vid * 2 - 1:02d}'] + df[f'st_{vid * 2:02d}']) / 2, '--', color=crv.get_color(),
            label=f"({vid * 2 - 1:d}+{vid * 2:d})/2")

ax.axhline(y=t_soil_deep, ls='--', color='#000000')
ax.text(df.index[0], t_soil_deep, "t_soil_deep", ha='left', va='top')

ax.legend(loc='upper left')
ax.set_ylabel("Soil temp [°C]")

ax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))

fig.tight_layout()
plt.show()
