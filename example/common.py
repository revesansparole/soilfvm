from dataclasses import dataclass

from saxton2006.table1 import (bubbling_pressure, matric_potential, theta_1500, theta_33, theta_sat, water_conductance,
                               water_conductance_sat)
from soilfvm.soil import VoxelBase

from constants import water_heat_capacity

soil_solid_density = 2.65
"""[g.cm-3] density of solid soil, gravels and the likes
"""


@dataclass
class Voxel(VoxelBase):
    kw_sat: float = None
    """[m3.MPa-1.s-1.m-1] soil water conductance at saturation
    """

    psi_b: float = None
    """[MPa] Bubbling potential.
    """

    kth_dry: float = None
    """[W.m-1.K-1] heat conductance of dry soil
    """

    vol_heat_cap_dry: float = None
    """[J.m-3.K-1] Volumetric heat capacity of dry soil
    """

    def kw(self, theta):
        """Water conductance.

        Args:
            theta (float): [m3.m-3] soil moisture

        Returns:
            (float): [m3.m-1.s-1.MPa-1]
        """
        # ksat = 3e-5  # [m3.MPa-1.s-1.m-1]
        kw_act = water_conductance(theta, self.theta_sat, self.theta_fc, self.theta_pwp, self.kw_sat)
        return self.kw_sat  # TODO need to use kw_act instead

    def psi(self, theta):
        """Water potential.

        Args:
            theta (float): [m3.m-3] soil moisture

        Returns:
            (float): [MPa]
        """
        psi_m = matric_potential(theta, self.theta_sat, self.theta_fc, self.theta_pwp, self.psi_b)
        psi_z = self.altitude * 1e3 * 9.81 * 1e-6

        return psi_m + psi_z

    def kth(self, theta=0.1):
        """Thermal conductivity of soil.

        Args:
            theta (float): [m3.m-3] soil moisture

        Returns:
            (float): [W.m-1.K-1]
        """
        k_water = 0.56 + 1.8e-3 * 20  # [W.m-1.K-1] from table 8.2

        return self.kth_dry + theta * k_water

    def heat_capacity(self, theta=0.1):
        """Heat capacity of voxel.

        Args:
            theta (float): [m3.m-3] soil moisture

        Returns:
            (float): [J.K-1]
        """
        return (self.vol_heat_cap_dry + theta * water_heat_capacity * 1e3) * self.volume

    def from_texture(self, clay_fraction=0.3, sand_fraction=0.3, om_fraction=0.01, stone_fraction=0):
        """Fill voxel parameters

        Args:
            clay_fraction (float): [g g-1] weight ratio (between 0 and 1) of clay as fraction of less than 2mm
            sand_fraction (float): [g g-1] weight ratio (between 0 and 1) of sand as fraction of less than 2mm
            om_fraction (float): [g g-1] weight ratio (between 0 and 1) of organic matter as fraction of less than 2mm
            stone_fraction (float): [m3 m-3] fraction of stones in volume between 0 and 1.

        Returns:
            (None): update object in place
        """
        self.theta_sat = theta_sat(clay_fraction, sand_fraction, om_fraction) * (1 - stone_fraction)
        self.theta_fc = theta_33(clay_fraction, sand_fraction, om_fraction) * (1 - stone_fraction)
        self.theta_pwp = theta_1500(clay_fraction, sand_fraction, om_fraction) * (1 - stone_fraction)
        self.theta_res = min(self.theta_pwp, 0.05)

        self.psi_b = bubbling_pressure(clay_fraction, sand_fraction, om_fraction)
        self.kw_sat = water_conductance_sat(self.theta_sat,
                                            self.theta_fc,
                                            self.theta_pwp) / 3600e3  # TODO [mm.h-1] to [m3.MPa-1.s-1.m-1]

        specific_heat_mineral = 0.87  # [J.g-1.K-1]
        bulk_density = soil_solid_density * (1 - self.theta_sat) * 1e3  # 1.3e3  # [kg.m-3]
        self.vol_heat_cap_dry = specific_heat_mineral * bulk_density * 1e3  # [J.m-3.K-1] from ex 8.12 without water

        k_clay = 2.3  # [W.m-1.k-1] just before example 8.2
        k_loam = 2  # [W.m-1.k-1] just before example 8.2
        k_sand = 5  # [W.m-1.k-1] just before example 8.2
        # TODO k_om = 0.25  # [W.m-1.K-1] from table 8.2
        loam_fraction = 1 - clay_fraction - sand_fraction

        self.kth_dry = (1 - self.theta_sat) * (clay_fraction * k_clay + sand_fraction * k_sand + loam_fraction * k_loam)
