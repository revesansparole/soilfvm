"""
Rain event evolution
====================

Infiltration of water in the soil
"""
from datetime import datetime

import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import pandas as pd
from soilfvm.soil_factory import column_1d
from soilfvm.water import balance

from common import Voxel

# get weather data
weather = pd.DataFrame([
    dict(date=datetime(2020, 6, 1, 0), rain=0),  # rain [m3.m-2.s-1]
    dict(date=datetime(2020, 6, 1, 3), rain=1.2e-6),
    dict(date=datetime(2020, 6, 1, 9), rain=0),
    dict(date=datetime(2020, 6, 3, 23), rain=0),
]).set_index('date')

# generate soil
soil_depth = 1  # [m] overall soil depth
nb = 5  # [#] number of extra horizons below surface horizon
soil = column_1d([0.05] + [(soil_depth - 0.05) / nb] * nb, Voxel)

for node, vox in soil.voxels():
    vox.from_texture(clay_fraction=0.2, sand_fraction=0.5)

surface_water = {edge: 0. for edge in soil.topo.out_edges('atm')}  # [m3] total amount of water stored on surface
surface_var = {edge: 0. for edge in soil.topo.out_edges('atm')}

theta = {node: vox.theta_fc for node, vox in soil.voxels()}
theta_var = {node: 0. for node, _ in soil.voxels()}

kroot = {node: [(0., 0.)] for node, _ in soil.voxels()}
runoff_th = 5e-3  # [m3.m-2] maximum amount of water storable on surface

dt = 10 * 60  # '10min' in [s]
weather = weather.resample('10min').fillna(method='ffill')

records = []
for date, row in weather.iterrows():
    # update state of model
    for node, _ in soil.voxels():
        theta[node] += theta_var[node]

    runoff = 0.  # [m3] amount of water flowing on top of soil
    for edge in soil.topo.out_edges('atm'):
        surf = soil.topo.edges[edge]['object'].surface
        surface_water[edge] += surface_var[edge] + row['rain'] * dt * surf
        storage_max = runoff_th * surf
        if surface_water[edge] > storage_max:
            runoff += surface_water[edge] - storage_max
            surface_water[edge] = storage_max

    # recompute new fluxes for this time step
    theta_var, surface_var, drainage = balance(soil, theta, surface_water, kroot, dt)

    records.append(dict(date=date,
                        surf=sum(surface_water.values()),
                        drainage=drainage,
                        runoff=runoff,
                        )
                   | dict(theta)
                   | {f'psi_{node:d}': vox.psi(theta[node]) for node, vox in soil.voxels()})

df = pd.DataFrame(records).set_index('date')

# plot result
fig, axes = plt.subplots(3, 1, sharex='all', figsize=(11, 8), squeeze=False)
ax = axes[0, 0]
ax.set_title(f"{weather.index[0].date().isoformat()}")

for vid, _ in sorted(soil.voxels()):
    ax.plot(df.index, df[vid], label=f"theta_{vid:d}")

vox = soil.topo.nodes[0]['object']
ax.axhline(y=vox.theta_sat, ls='--', color='#aaaaaa')

ax.legend(loc='upper left')
ax.set_ylabel("theta [m3.m-3]")

ax = axes[1, 0]
ax.plot(df.index, df['surf'])

ax = ax.twinx()
ax.plot(weather.index, weather['rain'], '--')

ax = axes[2, 0]
ax.plot(df.index, df['drainage'].cumsum(), label="drainage")
ax.plot(df.index, df['runoff'].cumsum(), label="runoff")
ax.legend(loc='upper left')

ax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))

fig.tight_layout()
plt.show()
